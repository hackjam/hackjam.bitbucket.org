/*-----------------------------------------------
 FSSlider - Unique Fullscreen Slider
 Version: 1.0 - (2011/01/28)
 Requires jQuery v1.4.2 or later 
 Author: Karim Hossenbux
 URL: http://themeforest.net/user/twi
-----------------------------------------------*/
(function ($) {
    var params = new Array;
    var order = new Array;
    var images = new Array;
    var interval = new Array;
    var imagePos = new Array;
    var appInterval = new Array;
    var squarePos = new Array;
    var reverse = new Array;
    var elem;
    $.fn.fsslider = function (options) {
        init = function (el) {
            order[el.id] = new Array();
            images[el.id] = new Array();
            imagePos[el.id] = 0;
            squarePos[el.id] = 0;
            reverse[el.id] = 1;
            elem = el;
            params[el.id] = $.extend({}, $.fn.fsslider.defaults, options);
            params[el.id].width = $(window).width();
            params[el.id].height = $(window).height();
            $.each($('#' + el.id + ' img'), function (i, item) {
                images[el.id][i] = $(item).attr('src');
                $(item).hide();
                $(item).next().hide();
            });
            $('#' + el.id).append("<div id='fss-content'></div>");
            $.render(el);
            if (params[el.id].navigation) {
                $.nav(el);
            }
            $.transition(el, 0);
            $.transitionCall(el);
        };
        $.render = function (el) {
            tWidth = sWidth = parseInt(params[el.id].width / params[el.id].spw);
            tHeight = sHeight = parseInt(params[el.id].height / params[el.id].sph);
            counter = sLeft = sTop = 0;
            tgapx = gapx = params[el.id].width - params[el.id].spw * sWidth;
            tgapy = gapy = params[el.id].height - params[el.id].sph * sHeight;
            for (i = 1; i <= params[el.id].sph; i++) {
                gapx = tgapx;
                if (gapy > 0) {
                    gapy--;
                    sHeight = tHeight + 1;
                } else {
                    sHeight = tHeight;
                }
                for (j = 1; j <= params[el.id].spw; j++) {
                    if (gapx > 0) {
                        gapx--;
                        sWidth = tWidth + 1;
                    } else {
                        sWidth = tWidth;
                    }
                    order[el.id][counter] = i + '' + j;
                    counter++;
                    $('#fss-content').append("<div class='fss-" + el.id + "' id='fss-" + el.id + i + j + "' style='width:" + sWidth + "px; height:" + sHeight + "px; float: left; position: absolute;'></div>");
                    $("#fss-" + el.id + i + j).css({
                        'background-position': -sLeft + 'px ' + (-sTop + 'px'),
                        'left': sLeft,
                        'top': sTop
                    });
                    sLeft += sWidth;
                }
                sTop += sHeight;
                sLeft = 0;
            }
            $('#' + el.id).append("<div id='fss-" + params[el.id].texture + "' class='fss-texture' style='width:" + params[el.id].width + "px;height:" + params[el.id].height + "px;'></div>");
        };
        $.transitionCall = function (el) {
            clearInterval(interval[el.id]);
            delay = params[el.id].delay + params[el.id].spw * params[el.id].sph * params[el.id].sDelay;
            interval[el.id] = setInterval(function () {
                $.transition(el);
            }, delay);
        };
        $.transition = function (el, direction) {
            $.effect(el);
            squarePos[el.id] = 0;
            appInterval[el.id] = setInterval(function () {
                $.appereance(el, order[el.id][squarePos[el.id]]);
            }, params[el.id].sDelay);
            if (typeof(direction) == "undefined") {
                imagePos[el.id]++;
            } else {
                if (direction == 'prev') {
                    imagePos[el.id]--;
                } else {
                    imagePos[el.id] = direction;
                }
            }
            var img = new Image();
            if (typeof(direction) != "undefined") {
                img.src = images[el.id][imagePos[el.id]];
            } else {
                img.src = images[el.id][0];
            }
            var w = img.width;
            var h = img.height;
            if (imagePos[el.id] == images[el.id].length) {
                imagePos[el.id] = 0;
            }
            if (imagePos[el.id] == -1) {
                imagePos[el.id] = images[el.id].length - 1;
            }
        };
        $.appereance = function (el, sid) {
            if (squarePos[el.id] == params[el.id].spw * params[el.id].sph) {
                clearInterval(appInterval[el.id]);
                return;
            }
            var img = new Image();
            img.src = images[el.id][imagePos[el.id]];
            var ratio = img.height / img.width;
            var browserheight = $(window).height();
            var browserwidth = $(window).width();
            if ((browserheight / browserwidth) > ratio) {
                var img_w = browserheight / ratio;
                var img_h = browserheight;
            } else {
                var img_w = browserwidth;
                var img_h = browserwidth * ratio;
            }
            var bgsize = img_w + 'px auto';
            $("#fss-" + el.id + sid).css({
                'background-size': bgsize,
                '-moz-background-size': bgsize,
                '-webkit-background-size': bgsize,
                '-o-background-size': bgsize,
            });
            $('#fss-' + el.id + sid).css({
                opacity: 0,
                'background-image': 'url(' + images[el.id][imagePos[el.id]] + ')'
            });
            $('#fss-' + el.id + sid).animate({
                opacity: 1
            }, 300);
            squarePos[el.id]++;
        };
        $.nav = function (el) {
            $('.fss-texture').append("<div id='fss-nav-" + el.id + "' style='position:absolute;z-index:200;bottom:10px;right:10px;'></div>");
            $('#fss-nav-' + el.id).append("<a href='#' id='fss-prev-" + el.id + "' class='fss-prev' style='margin-right:2px;'></a>");
            $('#fss-nav-' + el.id).append("<a href='#' id='fss-next-" + el.id + "' class='fss-next'></a>");
            $('#fss-prev-' + el.id).click(function (e) {
                e.preventDefault();
                $.transition(el, 'prev');
                $.transitionCall(el);
            }).mouseover(function () {
                $('#fss-nav-' + el.id).show();
            });
            $('#fss-next-' + el.id).click(function (e) {
                e.preventDefault();
                $.transition(el);
                $.transitionCall(el);
            }).mouseover(function () {
                $('#fss-nav-' + el.id).show();
            });
        };
        $.effect = function (el) {
            effA = ['rand', 'diagonal', 'lineal'];
            if (params[el.id].effect == '') {
                eff = effA[Math.floor(Math.random() * (effA.length))];
            } else {
                eff = params[el.id].effect;
            }
            order[el.id] = new Array();
            if (eff == 'rand') {
                counter = 0;
                for (i = 1; i <= params[el.id].sph; i++) {
                    for (j = 1; j <= params[el.id].spw; j++) {
                        order[el.id][counter] = i + '' + j;
                        counter++;
                    }
                }
                $.rand(order[el.id]);
            }
            if (eff == 'diagonal') $.diagonal(el);
            if (eff == 'lineal') $.lineal(el);
            reverse[el.id] *= -1;
            if (reverse[el.id] > 0) {
                order[el.id].reverse();
            }
        };
        $.rand = function (arr) {
            var i = arr.length;
            if (i == 0) return false;
            while (--i) {
                var j = Math.floor(Math.random() * (i + 1));
                var tempi = arr[i];
                var tempj = arr[j];
                arr[i] = tempj;
                arr[j] = tempi;
            }
        };
        $.lineal = function (el) {
            counter = 0;
            for (i = 1; i <= params[el.id].sph; i++) {
                for (j = 1; j <= params[el.id].spw; j++) {
                    order[el.id][counter] = i + '' + j;
                    counter++;
                }
            }
        };
        $.diagonal = function (el) {
            var n = params[el.id].sph;
            var m = params[el.id].spw;
            var c = 0;
            var to = to2 = from = 1;
            var dowhile = true;
            while (dowhile) {
                for (i = from; i <= to; i++) {
                    order[el.id][c] = i + '' + parseInt(to2 - i + 1);
                    c++;
                }
                to2++;
                if (to < n && to2 < m && n < m) to++;
                if (to < n && n >= m) to++;
                if (to2 > m) from++;
                if (from > to) dowhile = false;
            }
        };
        this.each(function () {
            init(this);
        });
        $(window).bind('resize', function () {
            $('#' + elem.id + ' .fss-texture').css({
                width: $(window).width(),
                height: $(window).height()
            });
            $.each($('.fss-' + elem.id), function () {
                tWidth = sWidth = parseInt($(window).width() / params[elem.id].spw);
                tHeight = sHeight = parseInt($(window).height() / params[elem.id].sph);
                counter = sLeft = sTop = 0;
                tgapx = gapx = $(window).width() - params[elem.id].spw * sWidth;
                tgapy = gapy = $(window).height() - params[elem.id].sph * sHeight;
                var img = new Image();
                img.src = images[elem.id][imagePos[elem.id]];
                var ratio = img.height / img.width;
                var browserheight = $(window).height();
                var browserwidth = $(window).width();
                if ((browserheight / browserwidth) > ratio) {
                    var img_w = browserheight / ratio;
                    var img_h = browserheight;
                } else {
                    var img_w = browserwidth;
                    var img_h = browserwidth * ratio;
                }
                var bgsize = img_w + 'px auto';
                for (i = 1; i <= params[elem.id].sph; i++) {
                    gapx = tgapx;
                    if (gapy > 0) {
                        gapy--;
                        sHeight = tHeight + 1;
                    } else {
                        sHeight = tHeight;
                    }
                    for (j = 1; j <= params[elem.id].spw; j++) {
                        if (gapx > 0) {
                            gapx--;
                            sWidth = tWidth + 1;
                        } else {
                            sWidth = tWidth;
                        }
                        order[elem.id][counter] = i + '' + j;
                        counter++;
                        $("#fss-" + elem.id + i + j).css({
                            'background-position': -sLeft + 'px ' + (-sTop + 'px'),
                            'left': sLeft,
                            'top': sTop,
                            'background-size': bgsize,
                            '-moz-background-size': bgsize,
                            '-webkit-background-size': bgsize,
                            '-o-background-size': bgsize,
                            'width': sWidth,
                            'height': sHeight
                        });
                        sLeft += sWidth;
                    }
                    sTop += sHeight;
                    sLeft = 0;
                }
            });
        });
    };
    $.fn.fsslider.defaults = {
        width: $(window).width(),
        height: $(window).height(),
        spw: 4,
        sph: 3,
        delay: 6000,
        sDelay: 70,
        effect: '',
        texture: 'cross',
        navigation: true
    };
})(jQuery);